<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
//        ajax requests
    Route::group(['prefix' => 'ajax'], function () {
        Route::group(['prefix' => 'home'], function () {
//            news
            Route::get('get_all_news', 'Ajax\NewsAjaxController@getAllNews');
//            calendar events
            Route::get('all_events', 'Ajax\CalendarAjaxController@getAllEvents');
            Route::post('all_events', 'Ajax\CalendarAjaxController@addEvent');
            Route::put('all_events/{id}', 'Ajax\CalendarAjaxController@updateEvent');
            Route::delete('all_events/{id}', 'Ajax\CalendarAjaxController@deleteEvent');
        });

        Route::group(['prefix' => 'users'], function () {
//        users
            Route::get('all_users', 'Ajax\UsersAjaxController@getAllUsers');
            Route::put('all_users/{id}', 'Ajax\UsersAjaxController@addToContacts');
//        contacts
            Route::get('contacts', 'Ajax\ContactsAjaxController@getAllContacts');
            Route::delete('contacts/{id}', 'Ajax\ContactsAjaxController@deleteContact');
//        contacts requests
            Route::get('contacts_requests', 'Ajax\ContactsRequestsAjaxController@getAllRequests');
            Route::put('contacts_requests/{id}', 'Ajax\ContactsRequestsAjaxController@acceptRequest');
            Route::delete('contacts_requests/{id}', 'Ajax\ContactsRequestsAjaxController@declineRequest');

        });
    });

//        http requests
    Route::group(['prefix' => 'home'], function () {
        Route::get('calendar', 'HomeController@toCalendar')->name('home.calendar');
        Route::get('news', 'HomeController@toNews')->name('home.news');
    });

    Route::group(['prefix' => 'events'], function () {
        Route::get('', 'EventsController@toNewEvent')->name('event.create');
        Route::post('', 'EventsController@saveEvent')->name('event.save');
        Route::get('{id}', 'EventsController@toExistingEvent')->name('event.edit');
        Route::put('{id}', 'EventsController@updateEvent')->name('event.update');
    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('', 'ProfileController@toProfile')->name('profile.show');
        Route::put('save_profile', 'ProfileController@saveProfile')->name('profile.save');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('all_users', 'UsersController@toAllUsers')->name('users.users');
        Route::get('contacts', 'UsersController@toContacts')->name('users.contacts');
        Route::get('contacts_requests', 'UsersController@toContactsRequests')->name('users.contacts.requests');
        Route::get('profile/{id}', 'UsersController@toOthersProfile')->name('users.profile');
    });

    Route::group(['prefix' => 'messages'], function () {
        Route::get('', 'MessagesController@toMessages')->name('messages.show');
    });

    Route::group(['prefix' => 'storage'], function () {
        Route::get('/', 'StorageController@toStorage')->name('storage.show');
    });

    Route::get('/', function () {
        return redirect()->route('home.calendar');
    });
});