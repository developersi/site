@extends('layouts.app')

@section('content')

<link href="{{ mix('css/home/fullcalendar.css') }}" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                {{--<div class="panel-heading">{{ \Carbon\Carbon::now() }}</div>--}}

                <div class="panel-body">
                    <div id="calendar"></div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Events form -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="exampleModalLabel" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="modal-title" class="control-label">Наименование события:</label>
                        <input id="modal-title" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="modal-start" class="control-label">Дата начала:</label>
                        <input id="modal-start" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="modal-end" class="control-label">Дата завершения:</label>
                        <input id="modal-end" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="modal-description" class="control-label">Описание:</label>
                        <textarea id="modal-description" class="form-control" rows="4"></textarea>
                    </div>
                    <div class="checkbox-allDay">
                        <input id="modal-allDay" type="checkbox">
                        <label for="modal-allDay">Длительное событие</label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="pull-left" id="modal-advanced">расширенные настройки</a>
                <button class="btn btn-primary btn-save"></button>
                <button class="btn btn-danger btn-delete">Удалить</button>
                <button class="btn btn-default btn-close">Отмена</button>
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('js/home/calendar.js') }}"></script>

@endsection
