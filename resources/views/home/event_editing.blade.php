@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Событие</div>

                <div class="panel-body">
                    Идентификатор: {{ Auth::user()->events()->find($id)->id }}<br>
                    Заголовок: {{ Auth::user()->events()->find($id)->title }}<br>
                    Начало: {{ Auth::user()->events()->find($id)->start }}<br>
                    Окончание: {{ Auth::user()->events()->find($id)->end }}<br>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
