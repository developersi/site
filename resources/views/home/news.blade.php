@extends('layouts.app')

@section('content')

<!-- Templates -->
<script id='newsCard' type="text/template">
    <a href="<%= url %>"><%= article %></a>
    <br>
    <span class="">Опубликовано: <%= created_at %></span>
</script>

<script id='nearestTemplate' type="text/template">
    <a><%= title %></a>
    <br>
    <span class="">Cтатус: <%= status %>, Сроки: <%= start %> - <%= end %></span>
</script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Новости</div>

                <div class="panel-body">
                    <div id="news"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('js/home/news.js') }}"></script>

@endsection
