@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Создание события</div>

                <div class="panel-body">
                    {!! Form::open(['route' => 'event.save', 'method' => 'post']) !!}

                    {!! Form::label('title', 'Название события', ['class' => '']) !!}
                    {!! Form::text('title', null,['class' => 'form-control']) !!}

                    {!! Form::label('start', 'Дата начала', ['class' => '', 'style' => 'margin-top:15px']) !!}
                    {!! Form::text('start', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}

                    {!! Form::label('end', 'Дата окончания', ['class' => '', 'style' => 'margin-top:15px']) !!}
                    {!! Form::text('end', \Carbon\Carbon::now()->addHour(), ['class' => 'form-control']) !!}

                    {!! Form::label('description', 'Описание', ['class' => '', 'style' => 'margin-top:15px']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}

                    {!! Form::checkbox('allDay', "Yes", false, ['class' => '', 'id' => 'allDay']) !!}
                    {!! Form::label('allDay', 'Длительное событие', ['class' => '', 'style' => 'margin-top:15px', 'aria-labelledby' => 'allDay']) !!}

                    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary form-control', 'style' => 'margin-top:20px']) !!}

                    {!! Form::token() . Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
