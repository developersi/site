@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Профиль</div>

                <div class="panel-body">
                    {{' Фамилия: ' . $profile->lastname }} <br>
                    {{' Имя: ' . $profile->name }} <br>
                    {{' Отчество: ' . $profile->patronymic }} <br>
                    {{' Должность: ' . $profile->post }} <br>
                    {{' Подразделение: ' . $profile->subdivision }} <br>
                    {{' Награды: ' . $profile->rewards }} <br>
                    {{' Достижения: ' . $profile->achievements }} <br>
                    {{' Дата начала работы: ' . $profile->date_of_employment }} <br>
                    {{' Отсчет стажа: ' . $profile->date_of_seniority_start }} <br>
                    {{' Интересующие теги: ' . $profile->interest_tags }} <br>
                    {{' Фотка: ' . $profile->photo }} <br>
                    {{' Цитата: ' . $profile->quote }} <br>
                    {{' Номер группы: ' . $profile->group_number }} <br>
                    {{' Староста: ' . $profile->praepostor }} <br>
                    {{' Куратор группы: ' . $profile->curator_of_group }} <br>
                    {{' Кафедра: ' . $profile->cathedra }} <br>
                    {{' Область обучения: ' . $profile->area_of_learning }} <br>
                    {{' Информация о дипломе: ' . $profile->diploma_info }} <br>
                    {{' Научная степень: ' . $profile->academic_degree }} <br>
                    {{' Научное звание: ' . $profile->academic_title }} <br>
                    {{' Читаемые дисциплины: ' . $profile->disciplines }} <br>
                    {{' Публикации: ' . $profile->publishing }}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
