@extends('layouts.app')

@section('content')

<!-- Templates -->
<script id='contactCard' type="text/template">
    <span>
        <button class="btn-delete btn btn-danger pull-right">
            <span>Убрать</span>
        </button>
        <span style="font-size:260%" class="glyphicon glyphicon-user pull-left" ></span>
        <span>Имя: <a href="profile/<%= id %>"><%= name %></a></span>
        <br>
        <span>Почта: <%= email %></span>
    </span>
</script>

<!--<script id='searchTemplate' type="text/template">-->
<!--    <div class="input-group">-->
<!--        <input id="searchBar" class="form-control">-->
<!--        <div class="input-group-btn">-->
<!--            <button class="btn btn-find btn-default" aria-haspopup="true" aria-expanded="false">-->
<!--                <span class="glyphicon glyphicon-search"></span>-->
<!--            </button>-->
<!--        </div>-->
<!--    </div>-->
<!--</script>-->

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class='panel-group'>
                    <div id="search">
                        <div class="input-group">
                            <input id="searchBar" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-find btn-default" aria-haspopup="true" aria-expanded="false">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="contacts"></div>
                        <div id="loading" style="margin-top:10px;text-align:center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('js/users/contacts.js') }}"></script>

@endsection
