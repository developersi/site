@extends('layouts.app')

@section('content')

<!-- Templates -->
<script id='requestCard' type="text/template">
    <span>
        <div class="btn-group pull-right">
            <button class="btn-accept btn btn-primary" enabled>
                <span>Принять</span>
            </button>
            <button class="btn-decline btn btn-danger" enabled>
                <span>Отклонить</span>
            </button>
        </div>
        <span style="font-size:260%" class="glyphicon glyphicon-user pull-left"></span>
        <span>Имя: <a href="profile/<%= id %>"><%= name %></a></span>
        <br>
        <span>Почта: <%= email %></span>
    </span>
</script>

<script id='searchTemplate' type="text/template">
    <div class="input-group">
        <input id="searchBar" class="form-control">
        <div class="input-group-btn">
            <button class="btn btn-find btn-default" aria-haspopup="true" aria-expanded="false">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </div>
    </div>
</script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class='panel-group'>
                    <div id="search">
                        <div class="input-group">
                            <input id="searchBar" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-find btn-default" aria-haspopup="true" aria-expanded="false">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="pendings"></div>
                        <div id="loading" style="margin-top:10px;text-align:center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('js/users/contacts_requests.js') }}"></script>

@endsection
