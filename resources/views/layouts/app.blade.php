<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
                        {{--{{ config('app.name', 'Laravel') }}--}}
                    {{--</a>--}}
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @if (!Auth::guest())
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <span style="margin-right:5px" class="glyphicon glyphicon-calendar pull-left"></span>
                                    <span>События</span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('home.calendar') }}">
                                            <span style="margin-right:15px">Календарь</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('home.news') }}">
                                            <span style="margin-right:15px">Новости</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <span style="margin-right:5px" class="glyphicon glyphicon-user pull-left"></span>
                                    <span>Пользователи</span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('users.users') }}">
                                            <span style="margin-right:15px">Все пользователи</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('users.contacts') }}">
                                            <span style="margin-right:15px">Контакты</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('users.contacts.requests') }}">
                                            <span style="margin-right:15px">Предложения</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="{{ route('messages.show') }}">
                                    <span style="margin-right:5px" class="glyphicon glyphicon-envelope pull-left"></span>
                                    <span>Сообщения</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('storage.show') }}">
                                    <span style="margin-right:5px" class="glyphicon glyphicon-cloud pull-left"></span>
                                    <span>Файлы</span>
                                </a>
                            </li>
                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{--{{ AC::test() }}--}}
                                    {{ Auth::user()->profile->lastname }}
                                    {{ Auth::user()->profile->name }}
                                    {{ Auth::user()->profile->patronymic }}
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('profile.show') }}">Профиль</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        {!! Form::open(['route' => 'logout', 'method' => 'post', 'id' => 'logout-form', 'style' => 'display: none;']) !!}
                                        {!! Form::token() . Form::close() !!}
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

</body>
</html>
