
contacts = require('../modules/contacts');

contacts.init({
    url: '/ajax/users/contacts',
    listPlace: '#contacts',
    searchPlace: '#search',
    paginatorPlace: '#loading'
});