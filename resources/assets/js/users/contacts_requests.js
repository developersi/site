
pendings = require('../modules/contacts-requests');

pendings.init({
    url: '/ajax/users/contacts_requests',
    listPlace: '#pendings',
    searchPlace: '#search',
    paginatorPlace: '#loading'
});