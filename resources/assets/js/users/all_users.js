
users = require('../modules/all-users');

users.init({
    url: '/ajax/users/all_users',
    listPlace: '#users',
    searchPlace: '#search',
    paginatorPlace: '#loading'
});