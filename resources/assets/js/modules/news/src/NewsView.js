
var _ = require('underscore');
var Backbone = require('backbone');
var ArticleView = require('./ArticleView');

var News = Backbone.View.extend({
    tagName: 'ul',
    initialize: function() {
        _.bindAll(  this,
        'nothingFound',
        'renderAll',
        'renderOne',
        'clear');
        this.collection.on('sync', this.renderAll);
    },
	renderAll: function() {
        this.clear();
        if (this.collection.length == 0) {
            this.nothingFound();
        } else {
            this.collection.each(this.renderOne, this);
        }
        return this;
	},
    renderOne: function(article) {
        var article = new ArticleView({model: article});
        this.$el.append(article.render().el);
    },
    nothingFound: function() {
        this.$el.html('<div style="text-align: center"><span style="font-style:oblique; color:grey">Ничего не найдено...</span></div>');
    },
    clear: function() {
        this.$el.empty();
    }
});

module.exports = News;
