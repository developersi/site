
var Backbone = require('../../backbone-csrf');

var News = Backbone.Collection.extend({
    url: '/ajax/home/get_all_news',
    initialize: function() {
        this.fetch();
    }
});

module.exports = News;
