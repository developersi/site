
var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');

var Article = Backbone.View.extend({
    tagName: 'li',
    className: 'list-group-item',
    template: _.template($('#newsCard').html()),
    initialize: function() {
        _.bindAll(  this,
        'render');

    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
});

module.exports = Article;
