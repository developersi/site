
var $ = require('jquery');
var ArticleView = require('./src/ArticleView');
var NewsCollection = require('./src/NewsCollection');
var NewsView = require('./src/NewsView');

newsPlace = $('#news');

var news = new NewsCollection;
var newsView = new NewsView({collection: news, el: newsPlace});
