
var Backbone = require('../../backbone-csrf');

var Contacts = Backbone.Collection.extend({
    url: '/ajax/users/contacts',
});

module.exports = Contacts;
