
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');
var ContactView = require('./ContactView');

var Contacts = Backbone.View.extend({
    tagName: 'ul',
    initialize: function() {
        _.bindAll(  this,
        'nothingFound',
        'renderAll',
        'renderOne',
        'clear');
        this.collection.on('add', this.renderOne);
        this.collection.on('reset', this.renderAll);
    },
	renderAll: function() {
        this.clear();
        if (this.collection.length == 0) {
            this.nothingFound();
        } else {
            this.collection.each(this.renderOne, this);
        }
        return this;
	},
    renderOne: function(user) {
        if (this.collection.length <= 1)
            this.clear();
        var userView = new ContactView({model: user});
        this.$el.append(userView.render().el);
    },
    nothingFound: function() {
        this.$el.html('<div style="text-align: center"><span style="font-style:oblique; color:grey">Ничего не найдено...</span></div>');
    },
    clear: function() {
        this.$el.empty();
    }
});

module.exports = Contacts;
