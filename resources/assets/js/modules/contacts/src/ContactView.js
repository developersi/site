
var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');

var Contact = Backbone.View.extend({
    tagName: 'li',
    className: 'list-group-item',
    template: _.template($('#contactCard').html()),
    events: {
        "click .btn-delete": "deleteContact",
    },
    initialize: function() {
        _.bindAll(  this,
        'render',
        'deleteBtnDisable',
        'deleteContact');
    },
    deleteContact: function() {
        this.model.collection.trigger('deleted');
        this.model.destroy(this.model.toJSON());
        this.deleteBtnDisable();
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        if (this.model.get('isContact'))
            this.deleteBtnDisable();
        return this;
    },
    deleteBtnDisable: function() {
        this.$el.find('.btn-delete').prop('disabled', true);
    }
});

module.exports = Contact;
