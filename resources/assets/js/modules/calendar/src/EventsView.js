
// TODO перейти на ES6

var $ = require('jquery');
var fullcalendar = require('fullcalendar');
var locale = require('../locale/ru-fixed');
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');
var moment = require('moment');
var EventView = require('./EventView');
var config = require('../config.json');

var View = Backbone.View.extend({
    initialize: function(){
        _.bindAll(this,
        "render",
        "select",
        "eventClick",
        "eventDropOrResize",
        "fcAddEvent",
        "fcChangeEvent",
        "fcDeleteEvent");
        this.$el = $(this.el);
        this.collection.on('sync', this.render);
        this.eventView = new EventView({el: '#exampleModal', collection: this.collection});
    },
    render: function() {
        this.$el.fullCalendar({
            timezone: 'local',
            header: {
                "left": "prev,today,next",
                "center": "title",
                "right": "month,agendaWeek,agendaDay,listWeek"
            },
            weekNumbers: true,
            weekNumbersWithinDays: true,
            weekNumberCalculation: "ISO",
            eventLimit: true,
            navLinks: true,
            selectable: true,
            editable: true,
            selectHelper: true,
            select: this.select,
            eventClick: this.eventClick,
            eventDrop: this.eventDropOrResize,
            eventResize: this.eventDropOrResize,
            events: this.collection.toJSON(),
            // windowResize: function(view) {
            //     alert('The calendar has adjusted to a window resize');
            // }
        });
    },
    select: function(start, end) {
        this.eventView.render(start, end);
    },
    eventClick: function(fcEvent) {
        this.eventView.model = this.collection.get(fcEvent.id);
        this.eventView.render();
    },
    eventDropOrResize: function(fcEvent) {
        this.collection.get(fcEvent.id).save({
            start: fcEvent.start,
            end: fcEvent.end
        });
    },
    fcAddEvent: function(event) {
        this.$el.fullCalendar('renderEvent', event.toJSON());
        this.$el.fullCalendar('unselect');
    },
    fcChangeEvent: function(event) {
        var fcEvent = this.$el.fullCalendar('clientEvents', event.get('id'))[0];
        fcEvent.start = event.get('start');
        fcEvent.end = event.get('end');
        fcEvent.title = event.get('title');
        fcEvent.allDay = event.get('allDay');
        this.$el.fullCalendar('updateEvent', fcEvent);
        this.$el.fullCalendar('rerenderEvent', fcEvent);
    },
    fcDeleteEvent: function(event) {
        this.$el.fullCalendar('removeEvents', event.get('id'));
    }
});

module.exports = View;
