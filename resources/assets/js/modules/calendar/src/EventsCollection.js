
var Backbone = require('../../backbone-csrf');
var Event = require('./EventModel.js');

var Collection = Backbone.Collection.extend({
    model: Event,
    url: '/ajax/home/all_events',
    initialize: function() {

        this.fetch();
    }
});

module.exports = Collection;
