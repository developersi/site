
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');

var View = Backbone.View.extend({
    events: {
        "click .btn-save" : "save",
        "click .btn-delete" : "delete",
        "click .btn-close" : "close"
    },
    initialize: function() {
        _.bindAll(this,
        "render",
        "close",
        "delete",
        "save",
        "deleteTrigger",
        "changeTrigger",
        "saveTrigger");
        this.$el = $(this.el);
    },
    render: function(start, end) {
        if (typeof(this.model) == 'undefined') {
            this.$el.find('.modal-title').text('Добавление события');
            this.$el.find('.btn-save').text('Добавить');
            this.$el.find('.btn-delete').hide();
            this.$el.find('#modal-start').val(start);
            this.$el.find('#modal-end').val(end);
            this.$el.find('#modal-title').val('');
            this.$el.find('#modal-description').val('');
            this.$el.find('#modal-allDay').prop('checked', false);
            this.$el.find('#modal-advanced').prop('href', '/events/');
        } else {
            this.$el.find('.modal-title').text('Редактирование события');
            this.$el.find('.btn-save').text('Сохранить');
            this.$el.find('.btn-delete').show();
            this.$el.find('#modal-start').val(this.model.get('start'));
            this.$el.find('#modal-end').val(this.model.get('end'));
            this.$el.find('#modal-title').val(this.model.get('title'));
            this.$el.find('#modal-description').val(this.model.get('description'));
            this.$el.find('#modal-allDay').prop('checked', this.model.get('allDay'));
            this.$el.find('#modal-advanced').prop('href', '/events/' + this.model.get('id'));
        }
        this.$el.modal('show');
    },
    close: function(){
        this.model = undefined;
        this.$el.modal('hide');
    },
    delete: function(){
        this.model.destroy({success: this.deleteTrigger});
        this.close();
    },
    save: function(){
         if (typeof(this.model) == 'undefined') {
             this.collection.create({
                 start: this.$el.find('#modal-start').val(),
                 end: this.$el.find('#modal-end').val(),
                 title: this.$el.find('#modal-title').val(),
                 description: this.$el.find('#modal-description').val(),
                 allDay: Number(this.$el.find('#modal-allDay').prop('checked'))
             }, {success: this.saveTrigger});
        } else {
            this.model.save({
                start: this.$el.find('#modal-start').val(),
                end: this.$el.find('#modal-end').val(),
                title: this.$el.find('#modal-title').val(),
                description: this.$el.find('#modal-description').val(),
                allDay: Number(this.$el.find('#modal-allDay').prop('checked'))
            }, {success: this.changeTrigger});
        }
        this.close();
    },
    saveTrigger: function(event) {
        this.trigger('fcAddEvent', event);
    },
    changeTrigger: function(event) {
        this.trigger('fcChangeEvent', event);
    },
    deleteTrigger: function(event) {
        this.trigger('fcDeleteEvent', event);
    }
});

module.exports = View;
