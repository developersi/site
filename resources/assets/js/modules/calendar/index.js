
var Events = require('./src/EventsCollection');
var EventsView = require('./src/EventsView');

var events = new Events;
var eventsView = new EventsView({el: '#calendar', collection: events});

eventsView.listenTo(eventsView.eventView, 'fcAddEvent', eventsView.fcAddEvent);
eventsView.listenTo(eventsView.eventView, 'fcChangeEvent', eventsView.fcChangeEvent);
eventsView.listenTo(eventsView.eventView, 'fcDeleteEvent', eventsView.fcDeleteEvent);




