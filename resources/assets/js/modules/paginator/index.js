
var _ = require('underscore');
var Backbone = require('../backbone-csrf');

var PaginatorModel = require('./src/PaginatorModel');
var PaginatorView = require('./src/PaginatorView');

var Paginators = new Backbone.Collection;
var PaginatorsViews = new Backbone.Collection;

module.exports = {
    Classes: {
        PaginatorModel: PaginatorModel,
        PaginatorView: PaginatorView
    },
    newPaginator: function(fields, proto, place) {
        var p = PaginatorModel.extend(proto, {
            defaults: _.extend({}, PaginatorModel.prototype.defaults, fields),
        }, proto);

        var pm = new p;

        Paginators.add(pm);
        PaginatorsViews.add(new PaginatorView({model: pm, el: place}));
    },
    getPaginators: function() {
        return Paginators;
    }
};
