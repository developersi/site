
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');

var Paginator = Backbone.Model.extend({
    defaults: {
        keywords: [],
        page: 1,
        perPage: 25,
        block: false,
    },
    initialize: function() {
        _.bindAll(  this,
        'nextPage',
        'loaded',
        'refresh',
        'find');
    },
    url: function(){
        return this.get('url');
    },
    nextPage: function() {
        if (this.get('block') == false) {
            this.set('page', this.get('page') + 1);
            this.fetch({data: this.toJSON(), success: this.loaded});
        }
    },
    find: function(keywords) {
        this.set('keywords', keywords);
        this.set('page', 1);
        this.fetch({data: this.toJSON(), success: this.refresh});
    },
    loaded: function() {
        this.trigger('loaded', this.get('data'));
        this.set('data', []);
    },
    refresh: function() {
        this.trigger('refresh', this.get('data'));
        this.set('data', []);
    }
});

module.exports = Paginator;
