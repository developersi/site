
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');

var PaginatorView = Backbone.View.extend({
    events: {
        "click": "loadMore",
    },
    initialize: function() {
        _.bindAll(  this,
        'loadMore',
        'render',
        'isLastPage');
        this.model.on('sync', this.isLastPage);
        this.render();
    },

    loadMore: function() {
        this.model.nextPage();
    },
    render: function() {
        this.$el.html('<a class="btn btn-link" style="font-style:oblique">Показать больше...</a>')
    },
    isLastPage: function() {
        if (this.model.get('block') == true)
            this.$el.hide();
        else
            this.$el.show();
    },
});

module.exports = PaginatorView;
