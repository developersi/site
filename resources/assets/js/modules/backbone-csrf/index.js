var $ = require('jquery');
var Backbone = require('backbone');

var oldSync = Backbone.sync;

Backbone.sync = function(method, model, options){
    options.beforeSend = function(xhr){
        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
    };
    return oldSync(method, model, options);
};

module.exports = Backbone;