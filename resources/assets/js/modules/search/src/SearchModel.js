
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');

var Search = Backbone.Model.extend({
    defaults: {
        keywords: [],
    },
    initialize: function() {
        _.bindAll(  this,
        'find');
        this.on('change', this.find);
    },
    find: function() {
        this.trigger('find', this.get('keywords'));
    },
});

module.exports = Search;
