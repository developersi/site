
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');

var SearchView = Backbone.View.extend({
    events: {
        "click .btn-find"   : "input",
        "change #searchBar" : "input",
        "input #searchBar"  : "input",
    },
    initialize: function() {
        _.bindAll(  this,
        'input',
        'render');
    },
    input: function() {
        this.model.set('keywords', this.$el.find('#searchBar').val().replace(/\s{2,}/g, ' ').split(' '));
    },
});

module.exports = SearchView;
