
var _ = require('underscore');
var Backbone = require('../backbone-csrf');
var SearchModel = require('./src/SearchModel');
var SearchView = require('./src/SearchView');

var Searches = new Backbone.Collection;
var SearchesViews = new Backbone.Collection;

module.exports = {
    Classes: {
        SearchModel: SearchModel,
        SearchView: SearchView
    },
    newSearch: function(fields, proto, place) {
        var s = SearchModel.extend(proto, {
            defaults: _.extend({}, SearchModel.prototype.defaults, fields),
        }, proto);

        var sm = new s;

        Searches.add(sm);
        SearchesViews.add(new SearchView({model: sm, el: place}));
    },
    getSearches: function() {
        return Searches;
    }
};
