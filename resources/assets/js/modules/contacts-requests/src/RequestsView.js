
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');
var RequestView = require('./RequestView');

var RequestsView = Backbone.View.extend({
    tagName: 'ul',
    initialize: function() {
        _.bindAll(  this,
        'nothingFound',
        'renderAll',
        'renderOne',
        'clear');
        this.collection.on('add', this.renderOne);
        this.collection.on('reset', this.renderAll);
    },
    renderAll: function() {
        this.clear();
        if (this.collection.isEmpty()) {
            this.nothingFound();
        } else {
            this.collection.each(this.renderOne, this);
        }
        return this;
    },
    renderOne: function(request) {
        var requestView = new RequestView({model: request});
        this.$el.append(requestView.render().el);
    },
    nothingFound: function() {
        this.$el.html('<div style="text-align: center"><span style="font-style:oblique; color:grey">Ничего не найдено...</span></div>');
    },
    clear: function() {
        this.$el.empty();
    }
});

module.exports = RequestsView;
