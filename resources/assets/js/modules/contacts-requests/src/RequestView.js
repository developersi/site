
var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');

var RequestView = Backbone.View.extend({
    tagName: 'li',
    className: 'list-group-item',
    template: _.template($('#requestCard').html()),
    events: {
        "click .btn-accept": "acceptContact",
        "click .btn-decline": "declineContact",
    },
    initialize: function() {
        _.bindAll(  this,
        'render',
        'acceptBtnDisable',
        'acceptContact',
        'declineBtnDisable',
        'declineContact');
    },
    acceptContact: function() {
        this.model.collection.trigger('accepted', this.model);
        this.model.save(this.model.toJSON());
        this.acceptBtnDisable();
        this.declineBtnDisable();
    },
    declineContact: function() {
        this.model.collection.trigger('declined', this.model);
        this.model.destroy(this.model.toJSON());
        this.acceptBtnDisable();
        this.declineBtnDisable();
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
    acceptBtnDisable: function() {
        this.$el.find('.btn-accept').prop('disabled', true);
    },
    declineBtnDisable: function() {
        this.$el.find('.btn-decline').prop('disabled', true);
    }
});

module.exports = RequestView;
