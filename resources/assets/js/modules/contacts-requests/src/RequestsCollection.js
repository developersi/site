
var Backbone = require('../../backbone-csrf');

var Requests = Backbone.Collection.extend({
    url: '/ajax/users/contacts_requests',
});

module.exports = Requests;
