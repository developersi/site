
var $ = require('jquery');
var Paginator = require('../paginator');
var Search = require('../search');

var items;
var itemsView;

var Collection = require('./src/RequestsCollection');
var View = require('./src/RequestsView');

module.exports = {
    init: function (params) {

        items = new Collection;
        itemsView = new View({collection: items, el: $(params.listPlace)});

        Paginator.newPaginator({},{url: params.url}, $(params.paginatorPlace));
        Search.newSearch({}, {}, $(params.searchPlace));

        Paginator.getPaginators().at(0).listenTo(Search.getSearches().at(0), 'find', Paginator.getPaginators().at(0).find);
        items.listenTo(Paginator.getPaginators().at(0), 'loaded', items.add);
        items.listenTo(Paginator.getPaginators().at(0), 'refresh', items.reset);

        Search.getSearches().at(0).find();
    },
};