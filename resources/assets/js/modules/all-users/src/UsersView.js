
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');
var UserView = require('./UserView');

var Users = Backbone.View.extend({
    tagName: 'ul',
    initialize: function() {
        _.bindAll(  this,
        'nothingFound',
        'renderAll',
        'renderOne',
        'clear');
        this.collection.on('add', this.renderOne);
        this.collection.on('reset', this.renderAll);
    },
    renderAll: function() {
        this.clear();
        if (this.collection.length == 0) {
            this.nothingFound();
        } else {
            this.collection.each(this.renderOne, this);
        }
        return this;
    },
    renderOne: function(user) {
        var userView = new UserView({model: user});
        this.$el.append(userView.render().el);
    },
    nothingFound: function() {
        this.$el.html('<div style="text-align: center"><span style="font-style:oblique; color:grey">Ничего не найдено...</span></div>');
    },
    clear: function() {
        this.$el.empty();
    }
});

module.exports = Users;
