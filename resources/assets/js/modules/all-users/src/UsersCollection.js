
var Backbone = require('../../backbone-csrf');

var Users = Backbone.Collection.extend({
    url: '/ajax/users/all_users',
});

module.exports = Users;
