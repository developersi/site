
var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('../../backbone-csrf');

var User = Backbone.View.extend({
    tagName: 'li',
    className: 'list-group-item',
    template: _.template($('#userCard').html()),
    events: {
        "click .btn-add": "addContact",
    },
    initialize: function() {
        _.bindAll(  this,
        'render',
        'addBtnDisable',
        'addContact');
    },
    addContact: function() {
        this.model.save(this.model.toJSON());
        this.addBtnDisable();
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        if (this.model.get('isContact'))
            this.addBtnDisable();
        return this;
    },
    addBtnDisable: function() {
        this.$el.find('.btn-add').prop('disabled', true);
    }
});

module.exports = User;
