
var Backbone = require('../../backbone-csrf');

var User = Backbone.Model.extend({
    defaults: {
        isContact: false,
    }
});

module.exports = User;
