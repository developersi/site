
var $ = require('jquery');
var Paginators = require('../paginator');
var Searches = require('../search');

var items;
var itemsView;

var Collection = require('./src/UsersCollection');
var View = require('./src/UsersView');

module.exports = {
    init: function (params) {

        items = new Collection;
        itemsView = new View({collection: items, el: $(params.listPlace)});

        Paginators.newPaginator({},{url: params.url}, $(params.paginatorPlace));
        Searches.newSearch({}, {}, $(params.searchPlace));

        Paginators.getPaginators().at(0).listenTo(Searches.getSearches().at(0), 'find', Paginators.getPaginators().at(0).find);
        items.listenTo(Paginators.getPaginators().at(0), 'loaded', items.add);
        items.listenTo(Paginators.getPaginators().at(0), 'refresh', items.reset);

        Searches.getSearches().at(0).find();
    },
}