<?php

use App\News;
use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $news = config('seeds.news');

        foreach ($news as $article) {
            $article_instance = News::firstOrCreate([
                'article' => $article['article'],
                'url' => $article['url'],
            ]);
        }

    }
}
