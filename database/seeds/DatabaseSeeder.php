<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaratrustSeederEdited::class);
        $this->call(UsersSeeder::class);
        $this->call(NewsSeeder::class);
    }
}
