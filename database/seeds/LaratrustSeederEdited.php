<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LaratrustSeederEdited extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        
        $config = config('laratrust_seeder_edited.role_structure');
        $userPermission = config('laratrust_seeder_edited.permission_structure');
        $mapPermission = collect(config('laratrust_seeder_edited.permissions_map'));

        foreach ($config as $key => $modules) {
//             Create a new role
            $role = \App\Role::create([
                'name' => $modules['name'],
                'display_name' => $modules['display_name'],
                'description' => $modules['description']
            ]);

//             Reading role permission modules
            foreach ($modules['permissions'] as $module => $value) {
                $permissions = explode(',', $value);
                foreach ($permissions as $p => $perm) {
                    $permissionValue = $mapPermission->get($perm);
                    $permission = \App\Permission::firstOrCreate([
                        'name' => $permissionValue . '-' . $module,
                        'display_name' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                        'description' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                    ]);
                    if (!$role->hasPermission($permission->name)) {
                        $role->attachPermission($permission);
                    } else {
                        $this->command->info($key . ': ' . $p . ' ' . $permissionValue . ' already exist');
                    }
                }
            }

//            Reading user permission modules
            foreach ($userPermission as $module => $value) {
                $permissions = explode(',', $value);
                foreach ($permissions as $p => $perm) {
                    $permissionValue = $mapPermission->get($perm);
                    $permission = \App\Permission::firstOrCreate([
                        'name' => $permissionValue . '-' . $module,
                        'display_name' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                        'description' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                    ]);
                }
            }
        }
    }
}
