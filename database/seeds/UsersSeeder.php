<?php

use App\Profile;
use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() // 50 users
    {

        $users = config('seeds.users');
        $profiles = config('seeds.profiles');
        $contacts = config('seeds.contacts');

        foreach ($users as $user) {
            $user_instance = User::firstOrCreate([
                'email' => $user['email'],
                'password' => bcrypt($user['password']),
                'remember_token' => str_random(10),
            ]);
            $user_instance->attachRole('abiturient');
        }

        foreach ($profiles as $profile) {

            $profile_instance = Profile::firstOrCreate([
                'name' => $profile['name'],
                'lastname' => $profile['lastname']
            ]);

            $profile_instance->user()->associate(User::find($profile_instance->id));
            $profile_instance->save();
        }

        foreach ($contacts as $contact) {
            User::find($contact['user_id'])->contactsRequested()->attach($contact['contact_id'], ['status' => $contact['status']]);
        }
    }
}
