<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('name')->default('Вася');
            $table->string('lastname')->default('Иванов');
            $table->string('patronymic')->default('Федорович');
            $table->string('post')->default('none');
            $table->string('subdivision')->default('none');
            $table->string('rewards')->default('none');
            $table->string('achievements')->default('none');
            $table->dateTime('date_of_employment')->default('2001-02-04 17:00');
            $table->dateTime('date_of_seniority_start')->default('2001-02-04 17:00');
            $table->string('interest_tags')->default('none');
            $table->string('photo')->default('none');
            $table->string('quote')->default('none');
            $table->string('group_number')->default('none');
            $table->boolean('praepostor')->default(0);
            $table->string('curator_of_group')->default('none');
            $table->string('cathedra')->default('none');
            $table->string('area_of_learning')->default('none');
            $table->string('diploma_info')->default('none');
            $table->string('academic_degree')->default('none');
            $table->string('academic_title')->default('none');
            $table->string('disciplines')->default('none');
            $table->boolean('publishing')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
