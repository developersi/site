<?php

namespace App\Traits;

Trait User {

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event');
    }

    /**
     * Доступ к подтвержденным и неподтвержденным
     * контактам, которых приглосил пользователь.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany    :   список контактов
     */
    public function contactsRequested()
    {
        return $this->belongsToMany('App\User', 'contact_user', 'user_id', 'contact_id');
    }

    /**
     * Доступ к подтвержденным и неподтвержденным
     * контактам, которые приглосили пользователя.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany    :   список контактов
     */
    public function contactsRecieved()
    {
        return $this->belongsToMany('App\User', 'contact_user', 'contact_id', 'user_id');
    }

}