<?php

namespace App\Traits;

Trait Event {

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

}