<?php

namespace App\Traits;

Trait Profile {

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}