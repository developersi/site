<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;

    use Traits\User;
    use LaratrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'pivot'
    ];

    /**
     * Добавление календарного события.
     *
     * @param $attrs    :   аттрибуты события
     * @return Event    :   новое событие
     */
    public function addEvent($attrs)
    {
        $newEvent = new Event($attrs);
        $this->events()
            ->save($newEvent);
        return $newEvent;
    }

    /**
     * Обновление события.
     *
     * @param $id       :   идентификатор события
     * @param $attrs    :   аттрибуты события
     * @return Event    :   обновленное событие
     */
    public function updateEvent($id, $attrs)
    {
        $updatedEvent = $this->events()
            ->find($id);
        $updatedEvent->update($attrs);
        return $updatedEvent;
    }

    /**
     * Удаление события.
     *
     * @param $id       :   идентификатор удаляемого события
     * @return Event    :   удаленное событие
     */
    public function deleteEvent($id)
    {
        $deletedEvent = $this->events()
            ->find($id);
        $this->events()
            ->find($id)
            ->delete();
        return $deletedEvent;
    }

    /**
     * Получение списка пользователей.
     * Порциальная выдача.
     *
     * @param $request  :   объект входного запроса
     * @return array    :   массив пользователей
     */
    public function getUsers($request)
    {
        $contentPerPage = $request->perPage;
        $page = $request->page;
        $user = $request->user();
        $keywords = $request->input('keywords');
        $columns = ['email'];

        $requested = $this->contactsRequested()
            ->searchPeople($keywords, $columns)
            ->get();
        $accepted = $this->contactsRecieved()
            ->searchPeople($keywords, $columns)
            ->get();

        $contacts = new Collection;
        $contacts = $contacts->merge($requested)
            ->merge($accepted);
        $users = User::where('id', '<>', $user->id)
            ->searchPeople($keywords, $columns)
            ->get();

        foreach ($contacts as $contact)
        {
            foreach ($users as $user)
            {
                if ($user->id == $contact->id)
                    $user->isContact = true;
            };
        };

        foreach ($users as $user)
        {
            $user->name = User::find($user->id)->profile->name;
        }

        $lastPage = floor(count($users) / $contentPerPage) + 1;
        $slice = array_slice($users->toArray(), $contentPerPage * ($page - 1), $contentPerPage);

        $block = false;
        if ($page >= $lastPage)
            $block = true;

        return [
            'data' => $slice,
            'block' => $block
        ];
    }

    /**
     * Получение приглашений в контакты.
     * Порциальная выдача.
     *
     * @param $request  :   объект входного запроса
     * @return array    :   массив приглашений
     */
    public function getRequests($request)
    {
        $contentPerPage = $request->perPage;
        $page = $request->page;
        $keywords = $request->input('keywords');
        $columns = ['email'];

        $pendings = $this->contactsRecieved()
            ->wherePivot('status', 0)
            ->searchPeople($keywords, $columns)
            ->get();

        foreach ($pendings as $pending)
        {
            $pending->name = User::find($pending->id)->profile->name;
        }

        $lastPage = floor(count($pendings) / $contentPerPage) + 1;
        $slice = array_slice($pendings->toArray(), $contentPerPage * ($page - 1), $contentPerPage);

        $block = false;
        if ($page >= $lastPage)
            $block = true;

        return [
            'data' => $slice,
            'block' => $block
        ];
    }

    /**
     * Получение подтвержденных контактов.
     *
     * @param $request  :   объект входного запроса
     * @return array    :   массив контактов
     */
    public function getContacts($request)
    {
        $contentPerPage = $request->perPage;
        $page = $request->page;
        $offset = $request->offset;
        $keywords = $request->input('keywords');
        $columns = ['email'];

        $requested = $this->contactsRequested()
            ->wherePivot('status', 1)
            ->searchPeople($keywords, $columns)
            ->get();
        $accepted = $this->contactsRecieved()
            ->wherePivot('status', 1)
            ->searchPeople($keywords, $columns)
            ->get();
        $contacts = new Collection;
        $contacts = $contacts->merge($requested)
            ->merge($accepted);


        foreach ($contacts as $contact)
        {
            $contact->name = User::find($contact->id)->profile->name;
        }

        $lastPage = floor((count($contacts) + $offset) / $contentPerPage) + 1;
        $slice = array_slice($contacts->toArray(), $contentPerPage * ($page - 1) - $offset, $contentPerPage);

        $block = false;
        if ($page >= $lastPage)
            $block = true;

        return [
            'data' => $slice,
            'block' => $block
        ];
    }

    /**
     * Приятие приглашения в контакты.
     *
     * @param $id   :   идентификатор приглашающего
     */
    public function acceptContact($id)
    {
        $this->contactsRecieved()
            ->updateExistingPivot($id, ['status' => 1]);
    }

    /**
     * Отклонение приглошения в контакты.
     *
     * @param $id   :   идентификатор приглашающего
     */
    public function declineContact($id)
    {
        $deletedContact = $this->contactsRecieved()
            ->wherePivot('status', 0)
            ->find($id);
        $this->contactsRecieved()
            ->detach($deletedContact);
    }

    /**
     * Приглашение пользователя в контакты.
     *
     * @param $id   :   идентификатор приглашаемого
     */
    public function addContact($id)
    {
        $newContact = User::find($id);
        $this->contactsRequested()
            ->attach($newContact);
    }

    /**
     * Удаление контакта.
     *
     * @param $id   :   идентификатор удаляемого контакта
     */
    public function deleteContact($id)
    {
        if ($this->contactsRequested()->wherePivot('status', 1)->find($id))
        {
            $deletedContact = $this->contactsRequested()
                ->wherePivot('status', 1)
                ->find($id);
            $this->contactsRequested()
                ->detach($deletedContact);
        }
        if ($this->contactsRecieved()->wherePivot('status', 1)->find($id))
        {
            $deletedContact = $this->contactsRecieved()
                ->wherePivot('status', 1)
                ->find($id);
            $this->contactsRecieved()
                ->detach($deletedContact);
        }
    }

    /**
     * Линза для создания запросов.
     * Ищет пользователей, у которых в профиле встречаются
     * ключевые слова.
     *
     * @param $query    :   цепочный запрос
     * @param $keywords :   ключевые слова
     * @param $columns  :   поля для поиска
     * @return mixed    :   цепочный запрос
     */
    public function scopeSearchPeople($query, $keywords, $columns)
    {
        if (count($keywords) == 0)
            return $query;
        else
        {
            return $query
                    ->where(function($query) use ($keywords, $columns)
                    {
                        foreach ($keywords as $keyword)
                        {
                            $query->where(function($query) use ($keyword, $columns)
                            {
                                foreach ($columns as $column)
                                {
                                    $query->orWhere($column, 'LIKE', '%'.$keyword.'%');
                                }
                            });
                        }
                    });
        }
    }
}
