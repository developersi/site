<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Переход к календарю.
     *
     * @return \Illuminate\Http\Response    :   вид страницы
     */
    public function toCalendar()
    {
        return view('/home/calendar');
    }

    /**
     * Переход к новостям.
     *
     * @return \Illuminate\Http\Response    :   вид страницы
     */
    public function toNews()
    {
        return view('/home/news');
    }
}
