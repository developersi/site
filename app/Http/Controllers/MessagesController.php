<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessagesController extends Controller
{
    /**
     * Переход к сообщениям.
     *
     * @return \Illuminate\Http\Response    :   вид страницы
     */
    public function toMessages() {
        return view('/messages/messages');
    }
}
