<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalendarAjaxController extends Controller
{

    /**
     * Получение всех календарных событий
     *
     * @param Request $request  :   входной запрос
     * @return mixed            :   коллекция календарных событий
     */
    public function getAllEvents(Request $request)
    {
        return $request->user()
            ->events;
    }

    /**
     * Создание календарного события
     *
     * @param Request $request  :   входной запрос
     * @return mixed            :   добавленное событие
     */
    public function addEvent(Request $request)
    {
        return $request->user()
            ->addEvent($request->all());
    }

    /**
     * Обновление календарного события
     *
     * @param Request $request  :   входной запрос
     * @param $id               :   идентификатор обновляемого события
     * @return mixed            :   обновленное событие
     */
    public function updateEvent(Request $request, $id)
    {
        return $request->user()
            ->updateEvent($id, $request->all());
    }

    /**
     * Удаление календарного события
     *
     * @param Request $request  :   входной запрос
     * @param $id               :   идентификатор удаляемого события
     * @return mixed            :   удаленное событие
     */
    public function deleteEvent(Request $request, $id)
    {
        return $request->user()
            ->deleteEvent($id);
    }
}
