<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersAjaxController extends Controller
{

    /**
     * Получение списка всех пользователей
     *
     * @param Request $request  :   объект входного запроса
     * @return mixed            :   коллекция пользователей
     */
    public function getAllUsers(Request $request)
    {
        return $request->user()
            ->getUsers($request);
    }

    /**
     * Отправка приглашения в контакты
     *
     * @param Request $request  :   объект входного запроса
     * @param $id               :   идентификатор потенциального контакта
     */
    public function addToContacts(Request $request, $id)
    {
        $request->user()
            ->addContact($id);
    }
}
