<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactsRequestsAjaxController extends Controller
{
    /**
     * Получение всех предложений дружбы
     *
     * @param Request $request  :   объект входного запроса
     * @return mixed            :   коллекция предложений
     */
    public function getAllRequests(Request $request)
    {
        return $request->user()
            ->getRequests($request);
    }

    /**
     * Принятие приглашения
     *
     * @param Request $request  :   объект входного запроса
     * @param $id               :   идентификатор потенциального контакта
     */
    public function acceptRequest(Request $request, $id)
    {
        $request->user()
            ->acceptContact($id);
    }

    /**
     * Отклонение приглашения
     *
     * @param Request $request  :   объект входного запроса
     * @param $id               :   идентификатор отклоняемого контакта
     */
    public function declineRequest(Request $request, $id)
    {
        $request->user()
            ->declineContact($id);
    }
}
