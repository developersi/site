<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactsAjaxController extends Controller
{
    /**
     * Получение списка контактов
     *
     * @param Request $request  :   входной запрос
     * @return mixed            :   коллекция контактов
     */
    public function getAllContacts(Request $request) {
        return $request->user()
            ->getContacts($request);
    }

    /**
     * Удаление контакта
     *
     * @param Request $request  :   входной запрос
     * @param $id               :   идентификатор удаляемого контакта
     */
    public function deleteContact(Request $request, $id) {
        $request->user()
            ->deleteContact($id);
    }
}
