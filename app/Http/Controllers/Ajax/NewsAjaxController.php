<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;

class NewsAjaxController extends Controller
{
    /**
     * Полечение последних новостей
     *
     * @return mixed    :   коллекция новостей
     */
    public function getAllNews()
    {
        return News::latest('created_at')
            ->take(20)
            ->get();
    }
}
