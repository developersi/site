<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StorageController extends Controller
{
    /**
     * Переход к хранилищу.
     *
     * @return \Illuminate\Http\Response    :   вид страницы
     */
    public function toStorage() {
        return view('/storage/storage');
    }
}
