<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventsController extends Controller
{
    /**
    * Переход к личному профилю пользователя.
    *
    * @return \Illuminate\Http\Response    :   вид страницы
    */
    public function toNewEvent()
    {
        return view('/events/new_event');
    }

    /**
     * Переход к личному профилю пользователя.
     *
     * @param $id
     * @return \Illuminate\Http\Response :   вид страницы
     */
    public function toExistingEvent($id)
    {
        return view('/events/existing_event')->with(['event' => Auth::user()->events()->find($id)]);
    }

    /**
     * Сохранение изменений личномого профиля пользователя.
     *
     * @param Request $request
     * @param $id
     * @return url :   страница профиля с обновленными данными
     */
    public function updateEvent(Request $request, $id)
    {
        $allDay = $request->input('allDay');
        $event = $request->user()->events()->find($id);

        if (isset($allDay) && $allDay === 'Yes') {
            $event->update([
                'title' => $request->input('title'),
                'start' => $request->input('start'),
                'end' => $request->input('end'),
                'description' => $request->input('description'),
                'allDay' => 1,
            ]);
        } else {
            $event->update([
                'title' => $request->input('title'),
                'start' => $request->input('start'),
                'end' => $request->input('end'),
                'description' => $request->input('description'),
                'allDay' => 0,
            ]);
        }
        return redirect()->route('home.calendar');
    }

    /**
     * Сохранение изменений личномого профиля пользователя.
     *
     * @param Request $request
     * @return url :   страница профиля с обновленными данными
     */
    public function saveEvent(Request $request)
    {
        $allDay = $request->input('allDay');

        if (isset($allDay) && $allDay === 'Yes') {
            $request->user()->events()->create([
                'title' => $request->input('title'),
                'start' => $request->input('start'),
                'end' => $request->input('end'),
                'description' => $request->input('description'),
                'allDay' => 1,
            ]);
        } else {
            $request->user()->events()->create([
                'title' => $request->input('title'),
                'start' => $request->input('start'),
                'end' => $request->input('end'),
                'description' => $request->input('description'),
                'allDay' => 0,
            ]);
        }
        return redirect()->route('home.calendar');
    }
}
