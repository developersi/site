<?php

namespace App\Http\Controllers;

use App\Contracts\AccessControlContract;
use Illuminate\Http\Request;

use AC;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    /**
     * Переход к личному профилю пользователя.
     *
     * @return \Illuminate\Http\Response    :   вид страницы
     */
    public function toProfile(Request $request)
    {
        return view('/profile/user_profile')->with(['profile' => Auth::user()->profile]);
    }

    /**
     * Сохранение изменений личномого профиля пользователя.
     *
     * @return url    :   вид страницы
     */
    public function saveProfile(Request $request)
    {
        $request->user()->update($request->except(['_token', '_method']));
        $request->user()->profile->update($request->except(['_token', '_method']));
        return redirect()->back();
    }
}
