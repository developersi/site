<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * Переход на страницу просмотра всех пользователей.
     *
     * @return \Illuminate\Http\Response    :   вид страницы
     */
    public function toAllUsers()
    {
        return view('/users/all_users');
    }

    /**
     * Переход на страницу просмотра контактов.
     *
     * @return \Illuminate\Http\Response    :   вид страницы
     */
    public function toContacts()
    {
        return view('/users/contacts');
    }

    /**
     * Переход на страницу просмотра предложений дружбы.
     *
     * @return \Illuminate\Http\Response    :   вид страницы
     */
    public function toContactsRequests()
    {
        return view('/users/contacts_requests');
    }

    /**
     * Переход на страницу профиля пользователя.
     *
     * @param $id                           :   идентификатор пользователя
     * @return \Illuminate\Http\Response    :   вид страницы
     */
    public function toOthersProfile($id)
    {
        return view('/users/others_profile', ['profile' => User::find($id)->profile]);
    }
}
