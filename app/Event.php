<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use Traits\Event;

    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at',
        'pivot'
    ];


    /**
     * Мутатор для даты начала события.
     * Конвертация для сохранения в базу данных.
     *
     * @param $date :   дата начала
     */
    public function setStartAttribute($date)
    {
        $this->attributes['start'] = Carbon::parse($date)->timezone('utc');
    }

    /**
     * Мутатор для даты конца события.
     * Конвертация для сохранения в базу данных.
     *
     * @param $date :   дата конца
     */
    public function setEndAttribute($date)
    {
        $this->attributes['end'] = Carbon::parse($date)->timezone('utc');
    }

    /**
     * Геттер для даты начала события.
     *
     * @return static   :   класс каробона
     */
    public function getStartAttribute()
    {
        return Carbon::parse($this->attributes['start'])->toIso8601String();
    }

    /**
     * Геттер для даты конца события.
     *
     * @return static   :   класс каробона
     */
    public function getEndAttribute()
    {
        return Carbon::parse($this->attributes['end'])->toIso8601String();
    }
}
