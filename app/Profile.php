<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    use Traits\Profile;

    protected $fillable = [
        'name'
    ];

    protected $hidden = [
        'pivot'
    ];

}
