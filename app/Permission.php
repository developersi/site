<?php

namespace App;

use Laratrust\LaratrustPermission;
use Laratrust\Traits\LaratrustPermissionTrait;

class Permission extends LaratrustPermission
{
     use LaratrustPermissionTrait;

}
