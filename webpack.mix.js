const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .js('resources/assets/js/home/calendar.js', 'public/js/home')
    .js('resources/assets/js/home/news.js', 'public/js/home')
    .js('resources/assets/js/users/all_users.js', 'public/js/users')
    .js('resources/assets/js/users/contacts.js', 'public/js/users')
    .js('resources/assets/js/users/contacts_requests.js', 'public/js/users')
    .styles(['node_modules/fullcalendar/dist/fullcalendar.css'], 'public/css/home/fullcalendar.css')
    .version();
