<?php

return [
    'role_structure' => [
        1 => [
            'name' => 'superadministrator',
            'display_name' => 'Суперадминистратор',
            'description' => '',
            'permissions' => [
                'users' => 'c,r,u,d',
                'acl' => 'c,r,u,d',
                'profile' => 'r'
            ],
        ],
        2 => [
            'name' => 'administrator',
            'display_name' => 'Администратор',
            'description' => '',
            'permissions' => [
                'users' => 'c,r,u,d',
                'profile' => 'r'
            ],
        ],
        3 => [
            'name' => 'abiturient',
            'display_name' => 'Абитуриент',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        4 => [
            'name' => 'student-spetsialist',
            'display_name' => 'Студент-специалист',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        5 => [
            'name' => 'student-bakalavr',
            'display_name' => 'Студент-бакалавр',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        6 => [
            'name' => 'student-magistrant',
            'display_name' => 'Студент-магистрант',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        7 => [
            'name' => 'aspirant',
            'display_name' => 'Аспирант',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        8 => [
            'name' => 'doktorant',
            'display_name' => 'Докторант',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        9 => [
            'name' => 'prepodavatel',
            'display_name' => 'Преподаватель',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        10 => [
            'name' => 'starshiy-prepodavatel',
            'display_name' => 'Старший преподаватель',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        11 => [
            'name' => 'dotsent',
            'display_name' => 'Доцент',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        12 => [
            'name' => 'professor',
            'display_name' => 'Профессор',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        13 => [
            'name' => 'prorektor',
            'display_name' => 'Проректор',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        14 => [
            'name' => 'nachalnik-uchebnogo-otdela',
            'display_name' => 'Начальник учебного отдела',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        15 => [
            'name' => 'spetsialist-uchebnogo-otdela',
            'display_name' => 'Специалист учебного отдела',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        16 => [
            'name' => 'pomoschnik-prorektora',
            'display_name' => 'Помощник проректора',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        17 => [
            'name' => 'rektor',
            'display_name' => 'Ректор',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        18 => [
            'name' => 'mladshiy-nauchniy-sotrudnik',
            'display_name' => 'Младший научный сотрудник',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        19 => [
            'name' => 'nauchniy-sotrudnik',
            'display_name' => 'Научный сотрудник',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        20 => [
            'name' => 'starshiy-nauchniy-sotrudnik',
            'display_name' => 'Старший научный сотрудник',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        21 => [
            'name' => 'nachalnik-upravleniya',
            'display_name' => 'Начальник управления',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        22 => [
            'name' => 'nachalnik-otdela',
            'display_name' => 'Начальник отдела',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        23 => [
            'name' => 'injener',
            'display_name' => 'Инженер',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        24 => [
            'name' => 'veduschiy-injener',
            'display_name' => 'Ведущий инженек',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        25 => [
            'name' => 'programmist',
            'display_name' => 'Программист',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        26 => [
            'name' => 'veduschiy-programmist',
            'display_name' => 'Ведущий программист',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        27 => [
            'name' => 'spetsialist-otdela',
            'display_name' => 'Специалист отдела',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        28 => [
            'name' => 'dokumentoved',
            'display_name' => 'Документовед',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        29 => [
            'name' => 'vipusknik',
            'display_name' => 'Выпускник',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        30 => [
            'name' => 'content-manager',
            'display_name' => 'Контент менеджер',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        31 => [
            'name' => 'content-manager-PDITR',
            'display_name' => 'Контент менеджер ПДИТР',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
        32 => [
            'name' => 'content-manager-razdela',
            'display_name' => 'Контент менеджер раздела',
            'description' => '',
            'permissions' => [
                'profile' => 'r'
            ],
        ],
    ],

    'permission_structure' => [
        'cru_user' => [
            'profile' => 'c,r,u'
        ],
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
