<?php

return [

    'users' => [
        1 => [
            'email' => 'q@q.q',
            'password' => '123123',
        ],
        2 => [
            'email' => 'w@q.q',
            'password' => '123123',
        ],
        3 => [
            'email' => 'e@q.q',
            'password' => '123123',
        ],
        4 => [
            'email' => 'a@q.q',
            'password' => '123123',
        ],
        5 => [
            'email' => 's@q.q',
            'password' => '123123',
        ],
        6 => [
            'email' => 'd@q.q',
            'password' => '123123',
        ],
        7 => [
            'email' => 'z@q.q',
            'password' => '123123',
        ],
        8 => [
            'email' => 'x@q.q',
            'password' => '123123',
        ],
        9 => [
            'email' => 'c@q.q',
            'password' => '123123',
        ],
        10 => [
            'email' => 'r@q.q',
            'password' => '123123',
        ],
        11 => [
            'email' => 'f@q.q',
            'password' => '123123',
        ],
        12 => [
            'email' => 'v@q.q',
            'password' => '123123',
        ],
        13 => [
            'email' => 't@q.q',
            'password' => '123123',
        ],
        14 => [
            'email' => 'g@q.q',
            'password' => '123123',
        ],
        15 => [
            'email' => 'b@q.q',
            'password' => '123123',
        ],
        16 => [
            'email' => 'y@q.q',
            'password' => '123123',
        ],
        17 => [
            'email' => 'h@q.q',
            'password' => '123123',
        ],
        18 => [
            'email' => 'n@q.q',
            'password' => '123123',
        ],
        19 => [
            'email' => 'u@q.q',
            'password' => '123123',
        ],
        20 => [
            'email' => 'j@q.q',
            'password' => '123123',
        ],
        21 => [
            'email' => 'm@q.q',
            'password' => '123123',
        ],
        22 => [
            'email' => 'i@q.q',
            'password' => '123123',
        ],
        23 => [
            'email' => 'k@q.q',
            'password' => '123123',
        ],
        24 => [
            'email' => 'q@w.w',
            'password' => '123123',
        ],
        25 => [
            'email' => 'a@w.w',
            'password' => '123123',
        ],
        26 => [
            'email' => 'z@w.w',
            'password' => '123123',
        ],
        27 => [
            'email' => 'w@w.w',
            'password' => '123123',
        ],
        28 => [
            'email' => 's@w.w',
            'password' => '123123',
        ],
        29 => [
            'email' => 'x@w.w',
            'password' => '123123',
        ],
        30 => [
            'email' => 'e@w.w',
            'password' => '123123',
        ],
        31 => [
            'email' => 'd@w.w',
            'password' => '123123',
        ],
        32 => [
            'email' => 'c@w.w',
            'password' => '123123',
        ],
        33 => [
            'email' => 'r@w.w',
            'password' => '123123',
        ],
        34 => [
            'email' => 'f@w.w',
            'password' => '123123',
        ],
        35 => [
            'email' => 'v@w.w',
            'password' => '123123',
        ],
        36 => [
            'email' => 't@w.w',
            'password' => '123123',
        ],
        37 => [
            'email' => 'g@w.w',
            'password' => '123123',
        ],
        38 => [
            'email' => 'b@w.w',
            'password' => '123123',
        ],
        39 => [
            'email' => 'y@w.w',
            'password' => '123123',
        ],
        40 => [
            'email' => 'h@w.w',
            'password' => '123123',
        ],
        41 => [
            'email' => 'n@w.w',
            'password' => '123123',
        ],
        42 => [
            'email' => 'u@w.w',
            'password' => '123123',
        ],
        43 => [
            'email' => 'j@w.w',
            'password' => '123123',
        ],
        44 => [
            'email' => 'm@w.w',
            'password' => '123123',
        ],
        45 => [
            'email' => 'i@w.w',
            'password' => '123123',
        ],
        46 => [
            'email' => 'k@w.w',
            'password' => '123123',
        ],
        47 => [
            'email' => 'o@w.w',
            'password' => '123123',
        ],
        48 => [
            'email' => 'l@w.w',
            'password' => '123123',
        ],
        49 => [
            'email' => 'p@w.w',
            'password' => '123123',
        ],
        50 => [
            'email' => 'q@e.e',
            'password' => '123123',
        ],
    ],

    'profiles' => [
        1 => [
            'name' => 'Иван',
            'lastname' => 'Петров'
        ],
        2 => [
            'name' => 'Петр',
            'lastname' => 'Петров'
        ],
        3 => [
            'name' => 'Аркадий',
            'lastname' => 'Савельев'
        ],
        4 => [
            'name' => 'Павел',
            'lastname' => 'Васильев'
        ],
        5 => [
            'name' => 'Василий',
            'lastname' => 'Аркадьев'
        ],
        6 => [
            'name' => 'Леонид',
            'lastname' => 'Моисеев'
        ],
        7 => [
            'name' => 'Вадим',
            'lastname' => 'Дмитриев'
        ],
        8 => [
            'name' => 'Евлампия',
            'lastname' => 'Ульянова'
        ],
        9 => [
            'name' => 'Евдокия',
            'lastname' => 'Прокофьева'
        ],
        10  => [
            'name' => 'Олег',
            'lastname' => 'Платонов'
        ],
        11  => [
            'name' => 'Марат',
            'lastname' => 'Назаров'
        ],
        12  => [
            'name' => 'Кирилл',
            'lastname' => 'Леонов'
        ],
        13  => [
            'name' => 'Захар',
            'lastname' => 'Игнатов'
        ],
        14  => [
            'name' => 'Григорий',
            'lastname' => 'Ефимов'
        ],
        15  => [
            'name' => 'Валентин',
            'lastname' => 'Витальев'
        ],
        16  => [
            'name' => 'Августина',
            'lastname' => 'Богданова'
        ],
        17  => [
            'name' => 'Виктория',
            'lastname' => 'Дамирова'
        ],
        18  => [
            'name' => 'Евгения',
            'lastname' => 'Евдокимова'
        ],
        19  => [
            'name' => 'Елизавета',
            'lastname' => 'Миленова'
        ],
        20  => [
            'name' => 'Надежда',
            'lastname' => 'Оксанова'
        ],
        21  => [
            'name' => 'Савелий',
            'lastname' => 'Радомиров'
        ],
        22  => [
            'name' => 'Сидор',
            'lastname' => 'Серафимов'
        ],
        23  => [
            'name' => 'Сергей',
            'lastname' => 'Султанов'
        ],
        24  => [
            'name' => 'Степан',
            'lastname' => 'Семенов'
        ],
        25  => [
            'name' => 'Святослав',
            'lastname' => 'Тарасов'
        ],
        26  => [
            'name' => 'Юрий',
            'lastname' => 'Эдгаров'
        ],
        27  => [
            'name' => 'Ярослав',
            'lastname' => 'Юнусов'
        ],
        28  => [
            'name' => 'Федор',
            'lastname' => 'Тихонов'
        ],
        29  => [
            'name' => 'Тимофей',
            'lastname' => 'Трофимов'
        ],
        30  => [
            'name' => 'Роман',
            'lastname' => 'Платонов'
        ],
        31  => [
            'name' => 'Марат',
            'lastname' => 'Карлов'
        ],
        32  => [
            'name' => 'Лаврентий',
            'lastname' => 'Максимов'
        ],
        33  => [
            'name' => 'Мирослав',
            'lastname' => 'Климов'
        ],
        34  => [
            'name' => 'Никита',
            'lastname' => 'Остапов'
        ],
        35  => [
            'name' => 'Остап',
            'lastname' => 'Потапов'
        ],
        36  => [
            'name' => 'Глеб',
            'lastname' => 'Гордеев'
        ],
        37  => [
            'name' => 'Геннадий',
            'lastname' => 'Емельянов'
        ],
        38  => [
            'name' => 'Герман',
            'lastname' => 'Давлатов'
        ],
        39  => [
            'name' => 'Григорий',
            'lastname' => 'Евсеев'
        ],
        40  => [
            'name' => 'Игнат',
            'lastname' => 'Зенонов'
        ],
        41  => [
            'name' => 'Анатолий',
            'lastname' => 'Борисов'
        ],
        42  => [
            'name' => 'Антон',
            'lastname' => 'Викторов'
        ],
        43  => [
            'name' => 'Всеволод',
            'lastname' => 'Алексеев'
        ],
        44  => [
            'name' => 'Герасим',
            'lastname' => 'Андреев'
        ],
        45  => [
            'name' => 'Андрей',
            'lastname' => 'Артемьев'
        ],
        46  => [
            'name' => 'Альберт',
            'lastname' => 'Булатов'
        ],
        47  => [
            'name' => 'Арсений',
            'lastname' => 'Васильев'
        ],
        48  => [
            'name' => 'Вячеслав',
            'lastname' => 'Даниилов'
        ],
        49  => [
            'name' => 'Денис',
            'lastname' => 'Еремеев'
        ],
        50  => [
            'name' => 'Игнат',
            'lastname' => 'Искандеров'
        ],
    ],

    'contacts' => [
        1 => [
            'user_id' => '2',
            'contact_id' => '1',
            'status' => 0,
        ],
        2 => [
            'user_id' => '3',
            'contact_id' => '1',
            'status' => 0,
        ],
        3 => [
            'user_id' => '4',
            'contact_id' => '1',
            'status' => 0,
        ],
        4 => [
            'user_id' => '5',
            'contact_id' => '1',
            'status' => 0,
        ],
        5 => [
            'user_id' => '6',
            'contact_id' => '1',
            'status' => 0,
        ],
        6 => [
            'user_id' => '7',
            'contact_id' => '1',
            'status' => 0,
        ],
        7 => [
            'user_id' => '8',
            'contact_id' => '1',
            'status' => 0,
        ],
        8 => [
            'user_id' => '9',
            'contact_id' => '1',
            'status' => 0,
        ],
        9 => [
            'user_id' => '10',
            'contact_id' => '1',
            'status' => 0,
        ],
        10 => [
            'user_id' => '11',
            'contact_id' => '1',
            'status' => 0,
        ],
        11 => [
            'user_id' => '12',
            'contact_id' => '1',
            'status' => 0,
        ],
        12 => [
            'user_id' => '13',
            'contact_id' => '1',
            'status' => 0,
        ],
        13 => [
            'user_id' => '14',
            'contact_id' => '1',
            'status' => 0,
        ],
        14 => [
            'user_id' => '15',
            'contact_id' => '1',
            'status' => 0,
        ],
        15 => [
            'user_id' => '16',
            'contact_id' => '1',
            'status' => 0,
        ],
        16 => [
            'user_id' => '17',
            'contact_id' => '1',
            'status' => 0,
        ],
        17 => [
            'user_id' => '18',
            'contact_id' => '1',
            'status' => 1,
        ],
    ],


    'news' => [
        1 => [
            'article' => '3 февраля - Выпуск специалистов Военмеха',
            'url' => 'http://www.voenmeh.ru/view8849',
            'created_at' => '2017-06-24 15:32:53',
        ],
        2 => [
            'article' => 'Всероссийская научная конференция',
            'url' => 'http://www.voenmeh.ru/view8843',
            'created_at' => '2017-06-24 15:32:53',
        ],
        3 => [
            'article' => 'ПРИГЛАШАЕМ К УЧАСТИЮ В IX ОБЩЕРОССИЙСКОЙ МОЛОДЕЖНОЙ НАУЧНО-ТЕХНИЧЕСКОЙ КОНФЕРЕНЦИИ «МОЛОДЕЖЬ. ТЕХНИКА. КОСМОС»',
            'url' => 'http://www.voenmeh.ru/view8831',
            'created_at' => '2017-06-24 15:32:53',
        ],
        4 => [
            'article' => '1,2 февраля - Защита магистерских диссертаций на факультете «Р»',
            'url' => 'http://www.voenmeh.ru/view8833',
            'created_at' => '2017-06-24 15:32:53',
        ],
        5 => [
            'article' => '27 января - Акция памяти «Блокадный свет», посвященная 73-й годовщине полного освобождения Ленинграда от фашистской блокады',
            'url' => 'http://www.voenmeh.ru/view8820',
            'created_at' => '2017-06-24 15:32:53',
        ],
        6 => [
            'article' => '29 января - В Военмехе провели олимпиаду',
            'url' => 'http://www.voenmeh.ru/view8811',
            'created_at' => '2017-06-24 15:32:53',
        ],
        7 => [
            'article' => '27 января - Мероприятие, посвященное полному снятию блокады',
            'url' => 'http://www.voenmeh.ru/view8789',
            'created_at' => '2017-06-24 15:32:53',
        ],
        8 => [
            'article' => 'Поздравление Ректора БГТУ блокадников и ветеранов БГТУ с Днем полного снятия Блокады Ленинграда',
            'url' => 'http://www.voenmeh.ru/view8769',
            'created_at' => '2017-06-24 15:32:53',
        ],
        9 => [
            'article' => 'С Днем снятия Блокады Ленинграда!',
            'url' => 'http://www.voenmeh.ru/view8765',
            'created_at' => '2017-06-24 15:32:53',
        ],
        10 => [
            'article' => 'БГТУ «ВОЕНМЕХ» им. Д.Ф. Устинова принял участие в выставке-форуме «ВУЗПРОМЭКСПО-2016»',
            'url' => 'http://www.voenmeh.ru/view8761',
            'created_at' => '2017-06-24 15:32:53',
        ],
        11 => [
            'article' => '26 января - День открытых дверей в БГТУ "ВОЕНМЕХ" им. Д.Ф.Устинова',
            'url' => 'http://www.voenmeh.ru/view8760',
            'created_at' => '2017-06-24 15:32:53',
        ],
        12 => [
            'article' => 'Образовательные семинары',
            'url' => 'http://www.voenmeh.ru/view8751',
            'created_at' => '2017-06-24 15:32:53',
        ],
        13 => [
            'article' => 'Студенческое самоуправление в СПб: итоги 2016',
            'url' => 'http://www.voenmeh.ru/view8743',
            'created_at' => '2017-06-24 15:32:53',
        ],
    ],
];