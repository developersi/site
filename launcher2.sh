#!/bin/bash

while getopts 'hsrpb' flag; do
    case "${flag}" in
    h)
        echo "launcher script all rights not reserved (c)."
        echo "-s : start httpd, mysql"
        echo "-r : restart httpd, mysql"
        echo "-p : stop httpd, mysql"
        echo "-b : backup project"
        ;;
    s)
        # start httpd, mysql
        echo "starting httpd..."
        sudo systemctl start httpd
        echo "starting mysql..."
        sudo systemctl start mysqld
        echo "done!"
        ;;

    r)
        # restart httpd, mysql
        echo "restarting httpd..."
        sudo systemctl restart httpd
        echo "restarting mysql..."
        sudo systemctl restart mysqld
        echo "done!"
        ;;

    p)
        # stop httpd, mysql
        echo "stoping httpd..."
        sudo systemctl stop httpd
        echo "stoping mysql..."
        sudo systemctl stop mysqld
        echo "done!"
        ;;

    b)
        # backup project directory
        echo "saving..."
        zip -r ~/backup-`date +%d-%m-%y-%R:%S`.zip ..
        echo "done!"
        ;;

    *)
        echo "unexpected option, run with -h flag."
        ;;

    esac
done
exit 0